
  let canvas = document.querySelector("canvas");
  let context = canvas.getContext('2d');


const scale = 3;
const width = 16;
const height = 18;
const scaledWidth = scale * width;
const scaledHeight = scale * height;
const walkLoop = [0, 1, 0, 2];
const frameLimit = 7;
const score = 0;

const FLAG_POSX = 545; 
const FLAG_POSY = 214;
const FLAG_DIMINTION = 60;

let flagtwoX = 650; 
let flagtwoY = 294; 
let flagoneX = 654;
let flagoneY = 110; 
let flag1_visable = true;
let flag2_visable = true;

let currentLoopIndex = 0;
let frameCount = 0;
let currentDirection = 0;
let speed = 2;
let scoreCount = 0;
if (score){
    scoreCount = score;
}


let gamesEnd = false;
let scoredPoint = false;


let character = new Image();
character.src = "assets/img/Green-16x18-spritesheet.png";

let screen1 = new Image();
screen1.src = "./assets/img/Win.jpg";

let screenWin = new Image();
screenWin.src = "assets/img/win.jpg";

let screen2 = new Image();
screen2.src = "assets/img/GO.png";

let collectFlag = new Image();
collectFlag.src = "assets/img/flag.png";




function GameObject(spritesheet, x, y, width, height) {
  this.spritesheet = spritesheet;
  this.x = x;
  this.y = y;
  this.width = width;
  this.height = height;
  this.mvmtDirection = "None";
}

// Default Player

let player = new GameObject(character, 111, 109, 300, 290);
let gameOver = new GameObject(screen2, 0, 0, 800, 500);
let gameWin = new GameObject(screenWin, 0, 0, 800, 500);
let winner = new GameObject(screen1, 0, 0, 800, 500);
let flag = new GameObject(collectFlag, 610, 337, 10, 37);




// GamerInput (Left, Right, Up, Down, MouseClicks)
function GamerInput(input) {
  this.action = input; // Hold the current input as a string
}

// Default GamerInput is set to None
let gamerInput = new GamerInput("None"); //No Input


function input(event) {
  // Take Input from the Player
 
  if (event.type === "keydown") {
      switch (event.keyCode) {
          case 37: // Left Arrow
              gamerInput = new GamerInput("Left");
              break; //Left key
          case 38: // Up Arrow
              gamerInput = new GamerInput("Up");
              break; //Up key
          case 39: // Right Arrow
              gamerInput = new GamerInput("Right");
              break; //Right key
          case 40: // Down Arrow
              gamerInput = new GamerInput("Down");
              break; //Down key
          case 83:
              speed = 4;
              break;
          default:
              gamerInput = new GamerInput("None"); //No Input
      }
  } else {
      gamerInput = new GamerInput("None");
      speed = 2;
  }
}




function update() {
  // console.log("Update");
  // Check Input
  if (gamerInput.action === "Up") {
      if (player.y < 0){
          console.log("player at top edge");
      }
      else{
          player.y -= speed; // Move Player Up
      }
      currentDirection = 1;
  } else if (gamerInput.action === "Down") {
      if (player.y + scaledHeight > canvas.height){
          console.log("player at bottom edge");
      }
      else{
          player.y += speed; // Move Player Down
      }
      currentDirection = 0;
  } else if (gamerInput.action === "Left") {
      if (player.x < 0){
          console.log("player at left edge");
      }
      else{
          player.x -= speed; // Move Player Left
      }
      currentDirection = 2;
  } else if (gamerInput.action === "Right") {
      if (player.x + scaledWidth > canvas.width){

      }
      else{
          player.x += speed; // Move Player Right
      }
      currentDirection = 3;
  } else if (gamerInput.action === "None") {
  }
}



function drawFrame(image, frameX, frameY, canvasX, canvasY) {
  context.drawImage(image,
                frameX * width, frameY * height, width, height,
                canvasX, canvasY, scaledWidth, scaledHeight);
}

function animate() {
  if (gamerInput.action != "None"){
      frameCount++;
      if (frameCount >= frameLimit) {
          frameCount = 0;
          currentLoopIndex++;
          if (currentLoopIndex >= walkLoop.length) {
              currentLoopIndex = 0;
          }
      }      
  }
  else{
      currentLoopIndex = 0;
  }
  drawFrame(player.spritesheet, walkLoop[currentLoopIndex], currentDirection, player.x, player.y);
}

function writeScore(){
  let scoreString = "score: " + scoreCount;
  context.font = '22px sans-serif';
  context.fillStyle="black";
  context.fillText(scoreString, 690, 60)
}

function draw() {
  context.clearRect(0,0, canvas.width, canvas.height);
  drawHealthbar();


  console.log("flag1_visable" + flag1_visable);
  console.log("flag2_visable" + flag2_visable);
  

  if(flag1_visable)
  {
    context.drawImage(collectFlag, flagoneX, flagoneY, 60, 60); 
  }
  if(flag2_visable)
  {
    context.drawImage(collectFlag, flagtwoX, flagtwoY, 60, 60); 
  }
  if(!gamesEnd){
    animate();
    writeScore();  
  }
  else {
    context.drawImage(screen2, 0, 0, 800, 500);
    // context.clear(canvas);
  }

  if (!flag1_visable && !flag2_visable) {
    context.drawImage(screenWin, 0, 0, 800, 500);
  }
}


var fillVal = 0;

function drawHealthbar() {
  var width = 1100;
  var height = 25;
  var max = 100;
  var val = 10;

  // Draw the background
  context.fillStyle = "#000000";
  context.clearRect(0, 0, canvas.width, canvas.height);
  context.fillRect(0, 0, width, height);

  // Draw the fill
  context.fillStyle = "#00FF00";
  context.fillRect(0, 0, fillVal * width, height);
  fillVal += 0.0005;

  if(fillVal >= 0.7
    ){
    gamesEnd = true;
  }
  else {
  // for X 545 + 60 = 605 if player for x between FLAG_POSX && FLAG_POSX + FLAG_DIMINTION
  // for Y 214 -60 = 274 if player for x between FLAG_POSX && FLAG_POSX + FLAG_DIMINTION
       // 10 , 10
          // 10 + 30 , 10 + 30 
          // console.log( "X >>" + (player.x >= flagoneX && player.x <= flagoneX + FLAG_DIMINTION));
          // console.log("Y >>" + (player.y >= flagoneY && player.y <= flagoneY - FLAG_DIMINTION));
          
    if( (player.x >= flagoneX && player.x <= flagoneX + FLAG_DIMINTION)
        && 
        (player.y >= flagoneY && player.y <= flagoneY + FLAG_DIMINTION)
        &&
        flag1_visable)
    {
      scoreCount ++;
      console.log("score");
      localStorage.setItem("score", scoreCount);
      flag1_visable = false;
    }
    if( (player.x >= flagtwoX && player.x <= flagtwoX + FLAG_DIMINTION)
        && 
        (player.y >= flagtwoY && player.y <= flagtwoY + FLAG_DIMINTION)
        &&
        flag2_visable)
    {
      scoreCount ++;
      console.log("score");
      localStorage.setItem("score", scoreCount);
      flag2_visable = false;
    }


  }
  // {
  //   if(fillVal < 1){
  //     // context.drawImage(screen1, 0, 0, 800, 500);
  //     // context.clear(canvas);

  //     scoreCount ++;
  //     console.log("score");
  //     localStorage.setItem("score", scoreCount);
  //   }
  // }
  // else if (fillVal > 1)
  // {
  //   // alert("GAME OVER");
  //   context.drawImage(screen2, 0, 0, 800, 500);
  //   context.clear(canvas);
}

var dynamic = nipplejs.create({
  color: 'red',
  zone: document.getElementById("joystick")
});


dynamic.on('added', function (evt, nipple) {
  //nipple.on('start move end dir plain', function (evt) {
  nipple.on('dir:up', function (evt, data) {
     //console.log("direction up");
     gamerInput = new GamerInput("Up");
  });
  nipple.on('dir:down', function (evt, data) {
      //console.log("direction down");
      gamerInput = new GamerInput("Down");
   });
   nipple.on('dir:left', function (evt, data) {
      //console.log("direction left");
      gamerInput = new GamerInput("Left");
   });
   nipple.on('dir:right', function (evt, data) {
      //console.log("direction right");
      gamerInput = new GamerInput("Right");
   });
   nipple.on('end', function (evt, data) {
      //console.log("mvmt stopped");
      gamerInput = new GamerInput("None");
   });
});

function movementY()
{
  gamerInput = new GamerInput("Up");
}
function movementX()
{
  gamerInput = new GamerInput("Left");
}
function movementB()
{
  gamerInput = new GamerInput("Right");
}
function movementA()
{
  gamerInput = new GamerInput("Down");
}
function stopClick()
{
  gamerInput = new GamerInput("None");
}



function gameloop()
{
  update();

  draw();

  window.requestAnimationFrame(gameloop);
}

window.requestAnimationFrame(gameloop);

// https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame

window.addEventListener('keydown', input);
// disable the second event listener if you want continuous movement
window.addEventListener('keyup', input);